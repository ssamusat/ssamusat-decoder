#!/bin/sh
#
# SatNOGS decoders Kaitai Struct compiling script
#
# Copyright (C) 2018-2019, 2023 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

KSC_VERSION="0.9"
IMAGE="${IMAGE:-librespace/kaitai:$KSC_VERSION}"
USER="$(id -u):$(id -g)"
VOLUME="$(pwd):/workdir"
DEFAULT_TARGET="python"
DEFAULT_OUTDIR="satnogsdecoders/decoder"
DEFAULT_FILE="ksy/*.ksy"

if ! docker pull "$IMAGE"; then
	echo "WARNING: Docker image was not updated!" >&2
fi

if [ -z "$1" ]; then
	docker run --rm --read-only \
	       -u "$USER" \
	       -v "$VOLUME" \
	       --read-only \
	       --rm \
	       --entrypoint /bin/sh \
	       "$IMAGE" \
	       -ec "
	       /usr/bin/ksc --version
	       /usr/bin/ksc --target '$DEFAULT_TARGET' --outdir '$DEFAULT_OUTDIR' $DEFAULT_FILE
	       echo 'KSYs compiled successfully!'
	       "
else
	docker run --rm --read-only \
	       -u "$USER" \
	       -v "$VOLUME" \
	       --read-only \
	       --rm \
	       "$IMAGE" \
	       "$@"
fi
